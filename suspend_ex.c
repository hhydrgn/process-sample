#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>

int main()
{
    int ret;

    printf("before suspend\n");
    ret = kill(getpid(), SIGSTOP);
    if (ret < 0)
        perror("kill()");
    printf("after suspend\n");

    return ret;
}
