**coding style**

http://httpd.apache.org/dev/styleguide.html

    $ indent -i4 -npsl -di0 -br -nce -d0 -cli0 -npcs -nfc1 -nut
    $ cppcheck --enable=all

**usage**

	$ make
	$ ./exec_ex1
	$ ./exec_ex2
	$ ./fork_ex
	$ ./fork_exec_ex [command]
	$ ./popen_ex
	$ ./ipc_sock_ex
	$ ./suspend_ex; fg
	$ ./system_ex1
	$ ./system_ex2 [command]

**fork_exec_ex example**

    $ ./fork_exec_ex cat
    Ctrl-D, kill [-TERM or -STOP] [child pid]
