#include <stdio.h>
#include <unistd.h>

int main()
{
    char *const cmd[] = {
        "echo", "Hello", "execvp()", NULL
    };
    execvp(cmd[0], cmd);
    perror("execlp()");
    return 1;
}
