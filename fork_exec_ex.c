#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/time.h>

#define debug_log(...) \
    do { \
        struct timeval tv; \
        gettimeofday(&tv, NULL); \
        printf("[%d][", getpid()); \
        printf("%4d.%03d", tv.tv_sec % 10000, tv.tv_usec / 1000); \
        printf("] "); \
        printf(__VA_ARGS__); \
    } while (0)

int main(int argc, char *argv[])
{
    debug_log("entered %s\n", __FILE__);

    int ret = EXIT_FAILURE;
    pid_t pid = fork();

    if (pid < 0) {
        perror("fork()");
        goto end;
    }
    else if (pid == 0) {
        if (argc > 1) {
            char **cmd = &argv[1];
            debug_log("fork exec(%s", cmd[0]);
            for (int i = 1; i < argc - 1; ++i) {
                printf(" %s", cmd[i]);
            }
            printf(")\n");
            execvp(cmd[0], cmd);
            perror("execvp()");
        }
        goto end;
    }
    debug_log("waitpid(%d)\n", pid);
    waitpid(pid, &ret, WUNTRACED);
    if (WIFEXITED(ret))
        debug_log("WEXITSTATUS: %d\n", WEXITSTATUS(ret));
    if (WIFSIGNALED(ret))
        debug_log("WTERMSIG: %d\n", WTERMSIG(ret));
    if (WIFSTOPPED(ret)) {
        debug_log("WSTOPSIG: %d\n", WSTOPSIG(ret));
        if (kill(pid, SIGCONT) < 0) {
            perror("kill()");
        }
    }
    ret = EXIT_SUCCESS;

  end:
    debug_log("exit %s return %d\n", __FILE__, ret);
    return ret;
}
