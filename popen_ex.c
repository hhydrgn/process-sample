#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>

#define debug_log(...) \
    do { \
        struct timeval tv; \
        gettimeofday(&tv, NULL); \
        printf("[%d][", getpid()); \
        printf("%4d.%03d", tv.tv_sec % 10000, tv.tv_usec / 1000); \
        printf("] "); \
        printf(__VA_ARGS__); \
    } while (0)


int main()
{
    debug_log("entered %s\n", __FILE__);

    int ret;
    size_t size = 256;
    char *line = (char *) calloc(size, sizeof(char));
    FILE *rpipe = NULL, *wpipe = NULL;

    if (!
        (rpipe =
         popen("echo Hello popen; ls; stdbuf -o0 ./suspend_ex;", "r"))) {
        perror("rpipe = popen()");
        goto end;
    }
    if (!(wpipe = popen("cat", "w"))) {
        perror("wpipe = popen()");
        goto end;
    }
    while (getline(&line, &size, rpipe) > 0) {
        fwrite(line, sizeof(char), size, wpipe);
        fflush(wpipe);
        memset(line, 0, size);
    }

  end:
    ret = (rpipe && wpipe) ? EXIT_SUCCESS : EXIT_FAILURE;
    if (rpipe)
        pclose(rpipe);
    if (wpipe)
        pclose(wpipe);
    if (line)
        free(line);

    debug_log("exit %s return %d\n", __FILE__, ret);
    return ret;
}
