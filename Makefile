targets = exec_ex1 exec_ex2 fork_ex fork_exec_ex popen_ex ipc_sock_ex suspend_ex system_ex1 system_ex2

all: $(targets)

%: %.c
	gcc -g -o $@ $<

clean:
	rm -f $(targets) socktest
