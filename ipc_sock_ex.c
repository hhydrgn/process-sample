#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/wait.h>
#include <sys/time.h>

#define debug_log(...) \
    do { \
        struct timeval tv; \
        gettimeofday(&tv, NULL); \
        printf("[%d][", getpid()); \
        printf("%4d.%03d", tv.tv_sec % 10000, tv.tv_usec / 1000); \
        printf("] "); \
        printf(__VA_ARGS__); \
    } while (0)

#define SOCK_PATH "socktest"

int main()
{
    debug_log("entered %s\n", __FILE__);

    struct sockaddr_un addr = {.sun_family = AF_UNIX,.sun_path = SOCK_PATH };
    char buf[256] = { 0 };
    int ret = EXIT_FAILURE, fd, len = sizeof(addr);

    unlink(addr.sun_path);

    pid_t pid = fork();

    if (pid < 0) {
        perror("fork()");
        goto end;
    }

    if (pid == 0) {
        if ((fd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0) {
            perror("child socket()");
            goto child_end;
        }

        int i = 0;
        do {
            if (i++ >= 5) {     /* maximum of retry */
                perror("connect()");
                goto child_end;
            }
            usleep(1);          /* wait parent's listen */
        } while (connect(fd, (struct sockaddr *) &addr, len));
        debug_log("connect (%d) ok, %d time%s\n", fd, i, i > 1 ? "s" : "");

        strcpy(buf, "Hello socket");
        if ((len = send(fd, buf, strlen(buf), 0)) < 0) {
            perror("send()");
            goto child_end;
        }
        debug_log("send %d bytes: %s\n", len, buf);
        ret = EXIT_SUCCESS;
      child_end:
        if (fd > 0) {
            shutdown(fd, SHUT_RDWR);
            close(fd);
        }
    }
    else {
        int listen_fd;

        if ((listen_fd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0) {
            perror("socket()");
            goto parent_end;
        }

        if (bind(listen_fd, (struct sockaddr *) &addr, len) < 0) {
            perror("bind()");
            goto parent_end;
        }
        debug_log("bind (%d) ok\n", listen_fd);

        if (listen(listen_fd, 5) < 0) {
            perror("listen()");
            goto parent_end;
        }
        debug_log("listen (%d) ok\n", listen_fd);

        fd_set fds;
        struct timeval tv = {.tv_sec = 5,.tv_usec = 0, };
        int tout;

        FD_ZERO(&fds);
        FD_SET(listen_fd, &fds);
        tout = select(listen_fd + 1, &fds, NULL, NULL, &tv);
        if (tout <= 0) {
            if (tout) {
                perror("select()");
            }
            else {
                debug_log("accept() timeout\n");
            }
            goto parent_end;
        }

        if ((fd = accept(listen_fd, (struct sockaddr *) &addr, &len)) < 0) {
            perror("accept()");
            goto parent_end;
        }
        debug_log("accept (%d) ok\n", fd);

        if ((len = recv(fd, buf, sizeof(buf), 0)) < 0) {
            perror("recv()");
            goto parent_end;
        }
        debug_log("recv %d bytes: %s\n", len, buf);

        if (pid != waitpid(pid, &ret, 0)) {
            perror("waitpid()");
        }
        ret = EXIT_SUCCESS;
      parent_end:
        if (fd > 0) {
            shutdown(fd, SHUT_RDWR);
            close(fd);
        }
        if (listen_fd > 0) {
            shutdown(listen_fd, SHUT_RDWR);
            close(listen_fd);
        }
        unlink(addr.sun_path);
    }

  end:
    debug_log("exit %s return %d\n", __FILE__, ret);
    return ret;
}
