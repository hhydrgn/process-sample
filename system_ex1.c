#include <stdio.h>
#include <stdlib.h>

int main()
{
    printf("entered %s\n", __FILE__);

    const char *cmd[] = {
        "/dev/null 2> /dev/null",
        "echo \"Hello system()\"",
        "while [ 0 ]; do sleep 1; done",
        "\0",
    };

    for (int i = 0; cmd[i][0] != '\0'; ++i) {
        printf("system(%s)\n", cmd[i]);
        int status = system(cmd[i]);
        printf("WEXITSTATUS: %d\n", WEXITSTATUS(status));
        printf("WTERMSIG: %d\n", WTERMSIG(status));
    }

    printf("exit %s\n", __FILE__);
    return 0;
}
