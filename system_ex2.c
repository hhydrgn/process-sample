#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>

#define debug_log(...) \
    do { \
        struct timeval tv; \
        gettimeofday(&tv, NULL); \
        printf("[%d][", getpid()); \
        printf("%4d.%03d", tv.tv_sec % 10000, tv.tv_usec / 1000); \
        printf("] "); \
        printf(__VA_ARGS__); \
    } while (0)


int main(int argc, char *argv[])
{
    debug_log("entered %s\n", __FILE__);

    char cmd[512] = { 0 };
    int offset = 0;

    for (int i = 1; i < argc; ++i) {
        offset += sprintf(cmd + offset, "%s ", argv[i]);
    }
    if (offset > 0)
        cmd[offset - 1] = 0;
    debug_log("call system(%s)\n", cmd);

    int status = system(cmd);
    if (WIFEXITED(status))
        debug_log("WEXITSTATUS: %d\n", WEXITSTATUS(status));
    if (WIFSIGNALED(status))
        debug_log("WTERMSIG: %d\n", WTERMSIG(status));

    debug_log("exit %s\n", __FILE__);
    return 0;
}
