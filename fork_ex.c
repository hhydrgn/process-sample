#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/time.h>

#define debug_log(...) \
    do { \
        struct timeval tv; \
        gettimeofday(&tv, NULL); \
        printf("[%d][", getpid()); \
        printf("%4d.%03d", tv.tv_sec % 10000, tv.tv_usec / 1000); \
        printf("] "); \
        printf(__VA_ARGS__); \
    } while (0)

int main()
{
    debug_log("entered %s\n", __FILE__);

    int ret = EXIT_FAILURE;
    pid_t pid = fork();

    if (pid < 0) {
        perror("fork()");
        goto end;
    }

    for (int i = 0; i < 5; ++i) {
        debug_log("%s: %d\n", pid ? "parent" : "child", i);
        if (pid)
            usleep(0);
    }

    if (pid) {
        if (pid != waitpid(pid, &ret, 0)) {
            perror("waitpid()");
        }
    }
    else {
        ret = EXIT_SUCCESS;
    }

  end:
    debug_log("exit %s (ppid: %d) return %d\n", __FILE__, getppid(), ret);
    return ret;
}
